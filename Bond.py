class Bond(object):
    def __init__(self, ISIN):
        self.isin = ISIN

    def GetISIN(self):
        return self.isin


bond1 = Bond('US123456B')
print(bond1.GetISIN())
